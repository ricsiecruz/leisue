import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Data } from './data-interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  dataList : Data[] = [];

  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get('../assets/data.json');
  }

  // getData(){
  //   return this.dataList;
  // }
}
