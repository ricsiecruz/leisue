import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DataService } from '../data.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  dataForm!: FormGroup;
  msg:String = '';

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataForm = new FormGroup({
      mobile_number: new FormControl(''),
      number_of_people: new FormControl(''),
      customer_name: new FormControl(''),
      queue_number: new FormControl('99'),
      isServed: new FormControl('false')
    })
  }

  add(){
    // if(this.dataForm.valid){
      this.dataService.dataList.push(this.dataForm.value);
      this.resetForm();
      console.log('this.dataService.studelost',this.dataService.getData())
      console.log("success", this.dataService.dataList.push(this.dataForm.value))
    // }
    //   else {
    //   this.msg = 'Please complete form'
    // }
  }
  
  resetForm(){
    console.log('reset',this.dataForm)
    this.dataForm.reset();
  }

}
