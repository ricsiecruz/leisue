import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

transform(items: any, searchItem: string): any[] {
    if(!items) return [];
    if(!searchItem) return items;

    searchItem = searchItem.toLowerCase();

    return items.filter( (item: string) => {
      return item.toLowerCase().includes(searchItem);
    });
  }

}