export interface Data {
    queue_number: number;
    customer_name: string;
    mobile_number: number;
    number_of_people: number;
    isServed: boolean;
}