import { Component } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Data } from './data-interface';
import { DataService } from './data.service';
import { ModalComponent } from './modal/modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'leisue-local';
  public jsonData: any;

  counter: any;
  searchText: any;
  dataList!: Data[];

  constructor(
    private dataService: DataService,
    private modalService: NzModalService
  ) {}

  ngOnInit(): void {
    this.dataService.getData().subscribe((data: any) => {
      this.jsonData = data;
      console.log(this.jsonData)
      
      let count = 0;
      for (const obj of this.jsonData) {
        if (obj.isServed === false) count++;
        this.counter = count
      }

      console.log(count); // 6
      console.log("aaa", this.counter)
    })

    
    // this.dataList = this.dataService.getData();

  }

  createComponentModal(): void {
    const modal = this.modalService.create({
      // nzTitle: 'Title',
      nzContent: ModalComponent
    });

  }
}
